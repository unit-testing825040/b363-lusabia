const { exchangeRates } = require("../src/util.js");

module.exports = (app) => {
  app.get("/", (req, res) => {
    return res.send({ data: {} });
  });

  app.get("/rates", (req, res) => {
    return res.send({
      rates: exchangeRates,
    });
  });

  app.post("/currency", (req, res) => {
    const rates = exchangeRates;

    if (typeof req.body.ex !== "object") {
      return res.status(400).send({
        error: "Ex should be an object.",
      });
    }

    if (Object.keys(req.body.ex).length === 0) {
      return res.status(400).send({
        error: "Ex should not be empty.",
      });
    }

    if (
      req.body.hasOwnProperty("name") &&
      req.body.hasOwnProperty("ex") &&
      req.body.hasOwnProperty("alias")
    ) {
      return res.status(200).send({
        success: "Complete input received.",
      });
    }

    if (!req.body.hasOwnProperty("name")) {
      return res.status(400).send({
        error: "Name is required.",
      });
    }

    if (typeof req.body.name !== "string") {
      return res.status(400).send({
        error: "Name should be string.",
      });
    }

    if (req.body.name === "") {
      return res.status(400).send({
        error: "Name should not be empty.",
      });
    }

    if (!req.body.hasOwnProperty("ex")) {
      return res.status(400).send({
        error: "Ex is required.",
      });
    }

    if (!req.body.hasOwnProperty("ex")) {
      return res.status(400).send({
        error: "Ex is required.",
      });
    }

    if (!req.body.hasOwnProperty("alias")) {
      return res.status(400).send({
        error: "No alias.",
      });
    }
    if (typeof req.body.alias !== "string") {
      return res.status(400).send({
        error: "Alias should be string.",
      });
    }

    rates.array.forEach((element) => {
      if (req.body.alias === element.alias) {
        return res.status(400).send({
          error: "Duplicate alias",
        });
      } else {
        return res.status(200).send({
          error: "No duplicate alias",
        });
      }
    });
  });
};
