const chai = require("chai");
const expect = chai.expect;
const http = require("chai-http");
chai.use(http);
const { exchangeRates } = require("../src/util");

describe("forex_api_test_suite", () => {
  it("test_api_get_rates_is_running", () => {
    chai
      .request("http://localhost:5001")
      .get("/rates")
      .end((err, res) => {
        expect(res).to.not.equal(undefined);
      });
  });

  it("test_api_get_rates_returns_200", (done) => {
    chai
      .request("http://localhost:5001")
      .get("/rates")
      .end((err, res) => {
        expect(res.status).to.equal(200);
        done();
      });
  });

  it("test_api_get_rates_returns_object_of_size_5", (done) => {
    chai
      .request("http://localhost:5001")
      .get("/rates")
      .end((err, res) => {
        expect(Object.keys(res.body.rates)).does.have.length(5);
        done();
      });
  });

  it("1-test-api-post-currency-returns-200-if-complete-input-given", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .type("json")
      .send({
        name: "dasdas",
        alias: "sds",
        ex: {
          peso: 0.47,
          usd: 0.0092,
          won: 10.93,
          yuan: 0.065,
        },
      })
      .end((err, res) => {
        expect(res.status).to.equal(200);
        done();
      });
  });

  it("2-test-api-post-currency-returns-400-if-no-name", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .type("json")
      .send({
        ex: {
          peso: 0.47,
          usd: 0.0092,
          won: 10.93,
          yuan: 0.065,
        },
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("3-test-api-post-currency-returns-400-if-name-is-not-string", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .type("json")
      .send({
        name: 123,
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("4-test-api-post-currency-returns-400-if-name-is-empty", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .type("json")
      .send({
        name: "",
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("5-test-api-post-currency-returns-400-if-no-ex", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .type("json")
      .send({
        name: "Rupees",
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("6-test-api-post-currency-returns-400-if-ex-is-not-object", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .type("json")
      .send({
        name: "Rupees",
        ex: "dasasd",
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("7-test-api-post-currency-returns-400-if-ex-is-empty", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .type("json")
      .send({
        name: "Rupees",
        ex: {},
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("8-test-api-post-currency-returns-400-if-no_alias", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .type("json")
      .send({
        name: "Rupees",
        ex: {},
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("9-test-api-post-currency-returns-400-if-name-is-not-string", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .type("json")
      .send({
        alias: 123,
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("10-test-api-post-currency-returns-400-if-alias-is-empty", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .type("json")
      .send({
        alias: "",
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("11-test-api-post-currency-returns-400-if-duplicate-alias", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .type("json")
      .send({
        usd: {
          name: "United States Dollar",
          ex: {
            peso: 50.73,
            won: 1187.24,
            yen: 108.63,
            yuan: 7.03,
          },
        },
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("11-test-api-post-currency-returns-200-if-no-dupes", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .type("json")
      .send({
        usd: {
          name: "United States Dollar",
          ex: {
            peso: 50.73,
            won: 1187.24,
            yen: 108.63,
            yuan: 7.03,
          },
        },
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });
}); //end of describe
