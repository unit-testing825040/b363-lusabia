function factorial(n) {
  if (n === 0) return 1;
  if (n === 1) return 1;
  return n * factorial(n - 1);
  // 5 * 4 * 3 * 2 * 1 = 120
}

function div_check(n) {
  if (n % 5 === 0) return true;
  if (n % 7 === 0) return true;

  return false;
}

const names = {
  Jaybee: {
    name: "Jaybee Lusabia",
    age: 29,
  },

  Freia: {
    name: "Freia Gargalicano",
    age: 20,
  },
};

module.exports = {
  factorial: factorial,
  div_check: div_check,
  names: names,
};
